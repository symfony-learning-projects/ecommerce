<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends AbstractController{

    //============================ SEARCH ============================

    /**
     * @Route("/categories", name="categories", methods="GET")
     */
    public function categories(){
        $categoriesRepository = $this->getDoctrine()->getRepository(Category::class);
        return $this->render('category/categories.html.twig', [
            'categories' => $categoriesRepository->findAll(),
        ]);
    }

    //============================ CREATE ============================

    /**
     * @Route("/categories/create", name="createCategory", methods={"GET","POST"})
     */
    public function createCategory(Request $request){

        $category = new Category();

        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($category);
            $entityManager->flush();
            return $this->redirectToRoute('categories');
        }

        return $this->render('category/createCategory.html.twig', [
            'category' => $category,
            'form' => $form->createView(),
        ]);
    }

    //============================ DELETE ============================

    /**
     * @Route("/categories/{id}/delete", name="deleteCategory",  methods="GET")
     */
    public function deleteCategory(Category $category) {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($category);
        $entityManager->flush();

        return $this->redirectToRoute('categories');
    }
}
