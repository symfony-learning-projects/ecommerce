<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', null, [
                'attr' => [
                    'placeholder' => 'Prénom',
                ],
                'label' => 'Prénom :'
            ])
            ->add('lastname', null, [
                'attr' => [
                    'placeholder' => 'Nom',
                ],
                'label' => 'Nom :'
            ])
            ->add('email', null, [
                'attr' => [
                    'placeholder' => 'Email',
                ],
                'label' => 'Email :'
            ])
            ->add('password', PasswordType::class, [
                'attr' => [
                    'placeholder' => 'Mot de passe',
                ],
                'label' => 'Mot de passe :',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
