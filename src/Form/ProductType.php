<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Product;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, [
                'attr' => [
                    'placeholder' => 'Titre',
                ],
                'label' => 'Titre :'
            ])
            ->add('description', null, [
                'attr' => [
                    'placeholder' => 'Description',
                ],
                'label' => 'Description :'
            ])
            ->add('price', null, [
                'attr' => [
                    'placeholder' => 'Prix',
                    'min' => '0',
                ],
                'label' => 'Prix :',
            ])
            ->add('quantity', null, [
                'attr' => [
                    'placeholder' => 'Quantité',
                    'min' => '0',
                ],
                'label' => 'Quantité :'
            ])
            ->add('linkPhoto', null, [
                'attr' => [
                    'placeholder' => 'Lien de la photo',
                ],
                'label' => 'Lien de la photo :'
            ])
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'label' => 'Catégorie :',
                'choice_label' => 'title',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
