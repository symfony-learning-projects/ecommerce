<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\Type("string")
     * @Assert\Length(
     *     max = 255,
     *     maxMessage = "Le prénom ne peut pas dépasser {{ limit }} caractères."
     * )
     * @Assert\NotBlank(
     *     message="Le nom ne peut pas être vide."
     * )
     * @ORM\Column(type="string", length=255)
     */
    private $firstname;

    /**
     * @Assert\Type("string")
     * @Assert\Length(
     *     max = 255,
     *     maxMessage = "Le nom ne peut pas dépasser {{ limit }} caractères."
     * )
     * @Assert\NotBlank(
     *     message="Le nom ne peut pas être vide."
     * )
     * @ORM\Column(type="string", length=255)
     */
    private $lastname;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank(
     *     message="L'email ne peut pas être vide."
     * )
     * @Assert\Email(
     *     message = "L'email '{{ value }}' n'est pas un email valide."
     * )
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @Assert\Type("string")
     * @Assert\NotBlank(message="Le mot de passe ne peut pas être vide.")
     * @Assert\Length(
     *     min="8",
     *     minMessage = "Le mot de passe doit faire minimum {{ limit }} caractères.",
     * )
     * @ORM\Column(type="string", length=100)
     */
    private $password;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }
}
